package ru.t1.dkozoriz.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.exception.AbstractException;

public final class EntityException extends AbstractException {


    public EntityException(String name) {
        super("Error! " + name + " not found.");
    }

    public EntityException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public EntityException(@NotNull final Throwable cause) {
        super(cause);
    }

    public EntityException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}