package ru.t1.dkozoriz.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.*;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.system.AccessDeniedException;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginErrorException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.util.CryptUtil;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.util.Date;


public final class AuthService implements IAuthService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public AuthService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public UserDto registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        return serviceLocator.getUserDtoService().create(login, password, email);
    }

    public String login(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = serviceLocator.getUserDtoService().findByLogin(login);
        if (user == null) throw new LoginErrorException();
        final boolean locked = user.getLocked();
        if (locked) throw new LoginErrorException();
        @Nullable final String hash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (hash == null) throw new LoginErrorException();
        if (!hash.equals(user.getPasswordHash())) throw new LoginErrorException();
        return getToken(user);
    }

    @Override
    @NotNull
    @SneakyThrows
    public SessionDto validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = serviceLocator.getPropertyService().getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDto session = objectMapper.readValue(json, SessionDto.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = serviceLocator.getPropertyService().getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (serviceLocator.getSessionDtoService().findById(session.getId()) == null) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void invalidate(@Nullable SessionDto session) throws Exception {
        if (session == null) return;
        serviceLocator.getSessionDtoService().remove(session);
    }

    @NotNull
    private String getToken(@NotNull final UserDto user) throws Exception {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDto session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = serviceLocator.getPropertyService().getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @Nullable
    private SessionDto createSession(@NotNull final UserDto user) throws Exception {
        @NotNull final SessionDto session = new SessionDto();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        serviceLocator.getSessionDtoService().add(session);
        return session;
    }

}