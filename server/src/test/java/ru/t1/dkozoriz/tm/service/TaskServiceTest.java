package ru.t1.dkozoriz.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.ITaskDtoService;
import ru.t1.dkozoriz.tm.component.Bootstrap;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.migration.AbstractSchemeTest;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.service.dto.UserDtoService;
import ru.t1.dkozoriz.tm.service.dto.business.ProjectDtoService;
import ru.t1.dkozoriz.tm.service.dto.business.TaskDtoService;

import java.util.ArrayList;
import java.util.List;


@Category(UnitCategory.class)
public class TaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IServiceLocator SERVICE_LOCATOR = new Bootstrap();

    @NotNull
    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String userId1 = "";

    @NotNull
    private static String userId2 = "";

    @NotNull
    private static String projectId1 = "";

    @NotNull
    private static String projectId2 = "";

    @NotNull
    private final List<TaskDto> taskList = new ArrayList<>();

    @NotNull
    private static final ITaskDtoService taskService = new TaskDtoService(SERVICE_LOCATOR);

    @NotNull
    private static final IUserDtoService userService = new UserDtoService(SERVICE_LOCATOR);

    @NotNull
    private static final IProjectDtoService projectService = new ProjectDtoService(SERVICE_LOCATOR);

    @BeforeClass
    public static void beforeTest() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        userId1 = userService.create("user1", "pass", Role.USUAL).getId();
        userId2 = userService.create("user2", "pass", Role.USUAL).getId();
        projectId1 = projectService.create(userId1, "project1", "desc1").getId();
        projectId2 = projectService.create(userId2, "project2", "desc2").getId();
    }

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDto task = new TaskDto("Task" + i, "Description" + i);
            if (i <= 10 / 2)
            {
                task.setUserId(userId1);
                task.setProjectId(projectId1);
            }
            else
            {
                task.setUserId(userId2);
                task.setProjectId(projectId2);
            }
            taskService.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final TaskDto task = new TaskDto();
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testAddUserOwnedTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final TaskDto task = new TaskDto();
        taskService.add(userId1, task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testUpdateTask() {
        @NotNull final TaskDto task = taskList.get(0);
        task.setName("taskUpdate");
        taskService.update(task);
        Assert.assertEquals(taskService.findById(task.getId()).getName(), "taskUpdate");
    }

    @Test
    public void testUpdateUserOwnedTask() {
        final int expectedNumberOfEntries = 0;
        taskService.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testClearTask() {
        final long expectedNumberOfEntries = 0;
        taskService.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testClearUserOwnedTask() {
        final long expectedNumberOfEntries = taskService.getSize(userId2);
        taskService.clear(userId1);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testFindAllTask() {
        final List<TaskDto> findAllTaskList = taskService.findAll();
        Assert.assertEquals(findAllTaskList.size(), taskService.getSize());
    }

    @Test
    public void testFindAllUserOwnedTask() {
        final List<TaskDto> findAllTaskList = taskService.findAll(userId1);
        Assert.assertEquals(findAllTaskList.size(), taskService.getSize(userId1));
    }

    @Test
    public void testFindByIdTask() {
        final String taskId = taskList.get(0).getId();
        final TaskDto task = taskService.findById(taskId);
        Assert.assertNotNull(task);
    }

    @Test
    public void testFindByIdUserOwnedTask() {
        final String taskId = taskList.get(0).getId();
        final TaskDto task = taskService.findById(userId1, taskId);
        Assert.assertNotNull(task);
    }

    @Test
    public void testRemoveTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String taskId = taskList.get(0).getId();
        final TaskDto task = taskService.findById(taskId);
        taskService.remove(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveUserOwnedTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String taskId = taskList.get(0).getId();
        final TaskDto task = taskService.findById(userId1, taskId);
        taskService.remove(userId1, task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveByIdTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String taskId = taskList.get(0).getId();
        taskService.removeById(taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveByIdUserOwnedTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String taskId = taskList.get(0).getId();
        taskService.removeById(userId1, taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testGetSizeTask() {
        Assert.assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testGetSizeUserOwnedTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2;
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(userId1));
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(userId2));
    }

    @Test
    public void testChangeStatusById() {
        @Nullable final TaskDto task = taskService
                .changeStatusById(userId1, taskList.get(0).getId(), Status.IN_PROGRESS);
        @Nullable final Status status = task.getStatus();
        Assert.assertEquals(Status.IN_PROGRESS, status);
    }

    @Test
    public void testChangeStatusByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .changeStatusById(userId, taskId, Status.IN_PROGRESS));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService
                .changeStatusById(userId1, id, Status.IN_PROGRESS));
        @Nullable final Status status = null;
        Assert.assertThrows(StatusEmptyException.class, () -> taskService
                .changeStatusById(userId1, taskId, status));
    }

    @Test
    public void testUpdateById() {
        @Nullable final TaskDto task = taskList.get(0);
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertEquals(name, taskService.updateById(userId1, task.getId(), name, description).getName());
        Assert.assertEquals(description, taskService.updateById(userId1, task.getId(), name, description).getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String taskId = taskList.get(0).getId();
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .updateById(userId, taskId, name, description));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService
                .updateById(userId1, id, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> taskService
                .updateById(userId1, taskId, name2, description));
    }

    @Test
    public void testCreateTask() {
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        @Nullable final TaskDto task = new TaskDto(name);
        task.setDescription(description);
        Assert.assertEquals(task.getName(), taskService.create(userId1, name, description).getName());
        Assert.assertEquals(task.getDescription(), taskService.create(userId1, name, description).getDescription());
    }

    @Test
    public void testCreateTaskNegative() {
        @Nullable final String userId = null;
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .create(userId, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> taskService
                .create(userId1, name2, description));
        @Nullable final String description2 = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService
                .create(userId1, name, description2));
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull TaskDto task1 = taskService.create(userId1, "task1", "description");
        @NotNull TaskDto task = taskList.get(0);
        @NotNull String taskId = task.getId();
        task1.setProjectId(projectId1);
        taskService.bindTaskToProject(userId1, projectId1, taskId);
        Assert.assertEquals(task1.getProjectId(), task.getProjectId());
    }

    @Test
    public void testBindTaskToProjectNegative() {
        @Nullable final String userId = null;
        @NotNull String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .bindTaskToProject(userId, projectId1, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> taskService
                .bindTaskToProject(userId1, null, taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService
                .bindTaskToProject(userId1, projectId1, null));
    }

    @Test
    public void testUnbindTaskToProject() {
        @NotNull TaskDto task = taskList.get(0);
        @NotNull String taskId = task.getId();
        task.setProjectId(projectId1);
        taskService.unbindTaskFromProject(userId1, projectId1, taskId);
        Assert.assertNull(taskService.findById(userId1, taskId).getProjectId());
    }

    @Test
    public void testUnbindTaskToProjectNegative() {
        @Nullable final String userId = null;
        @NotNull String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .unbindTaskFromProject(userId, projectId1, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> taskService
                .unbindTaskFromProject(userId1, null, taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService
                .unbindTaskFromProject(userId1, projectId1, null));
    }

    @After
    public void clearData() {
        taskList.clear();
        taskService.clear();
    }



}